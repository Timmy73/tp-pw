import { Component } from '@angular/core';
import { Movie } from '../home/movie';
import { Router } from '@angular/router';

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage {
  movie: Movie;
  constructor(private route: Router) {
    this.movie = this.route.getCurrentNavigation().extras.state as Movie; // paramMap.pipe(map(() => window.history.state));
    this.movie.poster_path = this.getImageUrl(this.movie.poster_path);
  }

  getImageUrl(image: string): string {
    return `https://image.tmdb.org/t/p/w780${image}`;
  }
}
