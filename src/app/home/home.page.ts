import { Component } from '@angular/core';
import { Movie, TMDBReponse } from './movie';
import { Router, NavigationExtras } from '@angular/router';
import { AlertController, LoadingController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { apiKey } from '../../tmdb';
import { async } from '@angular/core/testing';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage {
  movies: Promise<Movie[]>;
  movie: Movie;

  constructor(private readonly router: Router, public alertController: AlertController,
              private http: HttpClient, public loadingController: LoadingController) {
  }
  goToDetails(movie: Movie): void {
   const navigationExtras: NavigationExtras = {
     state: movie,
    };
    this.router.navigate(['/details'], navigationExtras);
  }
  getMovies(search: any): void {
    if (search.target.value.length < 3) {
      this.movies = Promise.resolve([]);
    } else {
      this.movies = this.searchMovies(search.target.value);
    }
  }

  async getRandomMovie() {
    this.discoverMovies().then(value => {this.movie = value[Math.floor(Math.random() * value.length)]; this.alertBox(); });
  }

  async alertBox() {
    const alert = await this.alertController.create({
      header: this.movie.title,
      message: this.movie.overview,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary'
        },
        {
          text: 'Let\'s go!',
          cssClass: 'primary',
          handler: () => {
            this.goToDetails(this.movie);
          }
        }
      ]});
    await alert.present();
  }

  private async askTMDB(api: string, params: object): Promise<Movie[]> {
    const loading = await this.loadingController.create({
      message: 'Launching missile',
      keyboardClose: false
    });
    await loading.present();
    const { results } = await this .http.get<TMDBReponse>(
    `https://api.themoviedb.org/3/${api}/movie`,
    { params: { api_key: apiKey, ...params } }
    ).toPromise();
    await loading.dismiss();
    return results;
    }

  private searchMovies(search: string): Promise<Movie[]> {
    return this.askTMDB('search', {query: search});
  }

  private discoverMovies(): Promise<Movie[]>  {
    return this.askTMDB('discover', { });
  }
}
